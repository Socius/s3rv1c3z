package main;

import com.mongodb.BasicDBObject;
import com.mongodb.BulkWriteOperation;
import com.mongodb.BulkWriteResult;
import com.mongodb.DB;

public class BulkThread<synchronised> implements Runnable {

	BulkWriteOperation bulk;
	DB db;
	boolean inserted = false;

	public BulkThread(DB db) {
		this.bulk = db.getCollection("weatherdata")
				.initializeUnorderedBulkOperation();
		this.db = db;
	}

	public void add(BasicDBObject obj) {
		synchronized (this) {
			// System.out.println("ins");
			bulk.insert(obj);
		}
		if (!inserted) {
			inserted = true;
		}
	}

	@Override
	public void run() {
		System.out.println("new Bulk Thread");
		long lastTime = System.currentTimeMillis();
		while (true) {
			if (lastTime + 1000 < System.currentTimeMillis()) {
				System.out.print("Timing: ");
				if (inserted) {
					long start = System.currentTimeMillis();
					synchronized (this) {
						BulkWriteResult result = bulk.execute();
						bulk = db.getCollection("weatherdata")
								.initializeUnorderedBulkOperation();
					}
					System.out.println((System.currentTimeMillis() - start)
							+ " ms");
				}
				lastTime = System.currentTimeMillis();
			}
		}
	}
}
