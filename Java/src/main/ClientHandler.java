package main;

import java.io.IOException;
import java.net.Socket;

import com.mongodb.BulkWriteOperation;

public class ClientHandler implements Runnable {
	private Socket client;
	BulkThread bulk;

	public ClientHandler(Socket client, BulkThread bulk) {
		this.client = client;
		this.bulk = bulk;
	}

	@Override
	public void run() {
		try {
			new XMLHandler(client, client.getInputStream(), bulk);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
