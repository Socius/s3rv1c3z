package main;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.UnknownHostException;

import com.mongodb.BulkWriteOperation;
import com.mongodb.DB;
import com.mongodb.Mongo;

public class Server {
	ServerSocket socket;
	int count;
	
	Mongo mongoClient = null;
	DB db;
	
	public Server() {
		System.out.println("new Server");
		socket = null;
		
		try {
			mongoClient = new Mongo("localhost", 27017);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		db = mongoClient.getDB("unwdmi");
		
		BulkThread bulkThread = new BulkThread(db);
		Thread t = new Thread(bulkThread);
		t.start();
		
		try {
			socket = new ServerSocket(7789);
			while(true) {
				new Thread(new ClientHandler(socket.accept(), bulkThread)).start();
				count++;
				if(count % 10 == 0){
					//System.out.println("clients: " + count);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
