package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;

import com.mongodb.BasicDBObject;
import com.mongodb.BulkWriteOperation;
import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

@SuppressWarnings("unused")
public class XMLHandler {
	NumberFormat nf = NumberFormat.getInstance();

	ArrayList<Double> tempValues = new ArrayList<Double>();

	BufferedReader reader;
	String curStationCode;

	BasicDBObject doc;
	private int uploadCount;
	
	Mongo mongoClient;
	DB readcon;
	BulkThread bulk;

	private Socket client;
	

	public XMLHandler(Socket client, InputStream inputStream, BulkThread bulk) {
		// System.out.println("XMLHandler");
		// this.database = database;
		this.client = client;
		try {
			mongoClient = new Mongo("localhost", 27017);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		readcon = mongoClient.getDB("unwdmi");
		
		this.bulk = bulk;

		reader = new BufferedReader(new InputStreamReader(inputStream));
		String line = "";
		try {
			reader.readLine();
			while ((line = reader.readLine()) != null) {
				// if (line.equals("</WEATHERDATA>"))
				// System.out.println("/weatherdata");
				if (!line.contains("WEATHERDATA")
						|| !line.contains("MEASUREMENT")) {

					if (line.indexOf(">") + 1 != line.length()) {
						if (line.contains("STN")) {
							if (doc == null)
								addStnCode(line.substring(
										line.indexOf(">") + 1,
										line.lastIndexOf("<")));
							else
								uploadDoc(line.substring(line.indexOf(">") + 1,
										line.lastIndexOf("<")));
						} else {
							if (doc != null) {
								appendDoc(line.substring(line.indexOf("<") + 1,
										line.indexOf(">")), line.substring(
										line.indexOf(">") + 1,
										line.lastIndexOf("<")));
							}
						}
					}
				}
			}
			end();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void appendDoc(String key, String value) {
		if (key != "TEMP") {
			if (value.length() != 0) {
				doc.append(key, value);
			} else {
				//System.out.println("fixing " + key + " for " + curStationCode);
				doc.append(key, value);
			}
		} else {
			addTemp(key, value);
		}
	}

	private void addTemp(String key, String string) {
		double value = Double.parseDouble(string);
		tempValues.add(value);
		if (tempValues.size() >= 30)
			tempValues.remove(0);
		double extrapolatedValue = calcExtrapolatedValue(tempValues);
		if (value > extrapolatedValue * 1.20
				|| value < extrapolatedValue * 0.80) {
			System.out.println("Temp is wrong");
			doc.append(key, extrapolatedValue);
		} else {
			doc.append(key, value);
		}
	}

	private double calcExtrapolatedValue(ArrayList<Double> values) {
		double divSum = 0;
		for (int i = 1; i < values.size(); i++) {
			divSum += values.get(i) - values.get(i - 1);
		}
		double divAvg = divSum / values.size();
		return values.get(values.size() - 1) + divAvg;
	}

	// Haalt de laatste 30 waardes van het betreffende station op
	// Slaat ze vervolgens op in een arraylist
	private String getExtrapolatedValue(String key) {
		ArrayList<String> values = new ArrayList<String>();
		DBCursor cursor = readcon.getCollection("weatherdata")
				.find(new BasicDBObject("stn", curStationCode), new BasicDBObject(key, 1))
				.sort(new BasicDBObject("_id", -1)).limit(30);
		if (cursor.count() > 0) {
			while (cursor.hasNext()) {
				String s = cursor.next().get(key).toString();
				values.add(s);
			}
			return Double.toString(calcExtrapolatedValueS(values));
		}
		return "";
	}

	// Berekent de geextrapoleerde waarde van de gegeven arraylist waardes
	private double calcExtrapolatedValueS(ArrayList<String> values) {
		double divSum = 0;
		for (int i = 1; i < values.size(); i++) {
			divSum += Double.parseDouble(values.get(i))
					- Double.parseDouble(values.get(i - 1));
		}
		double divAvg = divSum / values.size();
		return Double.parseDouble(values.get(values.size() - 1)) + divAvg;
	}

	// Upload data en stuurt nummer van het volgende station door
	private void uploadDoc(String newStn) {
		//db.getCollection("weatherdata").insert(doc);
		bulk.add(doc);
		doc = null;
		if (stationExcists(newStn)) {
			curStationCode = newStn;
			doc = new BasicDBObject("stn", newStn);
			//doInfoQuery(newStn);
		} else
			System.out.println("Unknown station");

	}

	// Laat om de honderd uploads data zien.
	private void doInfoQuery(String newStn) {
		if (uploadCount >= 100) {
			uploadCount = 0;
			DBObject obj = (DBObject) readcon.getCollection("weatherdata").find()
					.sort(new BasicDBObject("date", -1))
					.sort(new BasicDBObject("time", -1)).limit(1).next();
			System.out.println("Query: " + obj);
		}
		uploadCount++;

	}

	// Zet stn in de curStationCode variable
	private void addStnCode(String stn) {
		if (stationExcists(stn)) {
			curStationCode = stn;
			doc = new BasicDBObject("stn", stn);
		} else
			System.out.println("Unknown station");
	}

	private boolean stationExcists(String stn) {
		BasicDBObject query = new BasicDBObject("stn", stn);
		return readcon.getCollection("stations").find(query) != null;
	}

	// Pakt tot op laatste 30 temperaturen van stn
	// Pakt daar het gemiddelde van en checkt of temp hier minder dan 20% van
	// afwijkt
	// Als deze afwijkt stuur dan het gemiddelde terug, anders temp terug
	// sturen.
	private String checkAndCorrectTemp(String stn, String temp) {

		double totalDifference = 0;
		int amount = 0;
		DBObject lastObj = null;
		double last = 0;
		DBCursor cursor = readcon.getCollection("weatherdata")
				.find(new BasicDBObject("stn", stn))
				.sort(new BasicDBObject("_id", 1)).limit(30);
		while (cursor.hasNext()) {
			if (lastObj != null) {
				DBObject iteratorNext = cursor.next();
				try {
					last = nf.parse(iteratorNext.get("TEMP").toString())
							.doubleValue();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				totalDifference += Double.parseDouble(iteratorNext.get("TEMP")
						.toString())
						- Double.parseDouble(lastObj.get("TEMP").toString());
				lastObj = iteratorNext;
			} else {
				lastObj = cursor.next();
			}
			amount++;
		}

		// Bereken extrapolatie en afwijkingen
		double averageDiff = totalDifference / amount;
		double extrapolated = averageDiff + last;
		double mintwenty = 0;
		double plustwenty = 0;
		try {
			mintwenty = (0.80 * nf.parse(temp).doubleValue());
			plustwenty = (1.2 * nf.parse(temp).doubleValue());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (!(extrapolated >= mintwenty) && !(extrapolated <= plustwenty)) {
			System.out.println("adjusted temperature to: " + extrapolated
					+ ", from, " + temp);
			return String.valueOf(extrapolated);
		}
		return temp;
	}

	private void end() {
		try {
			System.out.println("disconnect");
			reader.close();
			client.close();
			mongoClient.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
